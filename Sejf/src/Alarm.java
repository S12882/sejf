import java.util.Scanner;


public class Alarm {
	String pin; 
	AlarmListener listeners; 
		
	Alarm(String pin){
		this.pin = pin;
	}
	
	public void addListener(AlarmListener listeners){
		AlarmListener A = new Police();
		this.listeners = listeners;
		
	}
	
	public void removeLister(AlarmListener listeners){
	
	}
	
	public void enterPin(String pin){
		
		
		if(this.pin != pin){
			Alarm.wrongEnteredPin();
	 } else {
		    Alarm.correctEnteredPin();
		
	}
				
}
	
	
    private static void wrongEnteredPin(){
		AlarmListener A = new Police();
		EnteredPinEvent event = new EnteredPinEvent();
		A.AlarmTurnedOn(event);
    	
	}
       
    private static void correctEnteredPin(){
    	AlarmListener A = new Police();
		EnteredPinEvent event = new EnteredPinEvent();
		A.AlarmTurnedOff(event);
	
	}

}
